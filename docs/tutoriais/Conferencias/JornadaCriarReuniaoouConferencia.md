
O presente documento tem como objetivo orientar o usuário sobre como criar reuniões ou conferências, seja uma etapa municipal, estadual ou livre dentro da plataforma Brasil Participativo.

Para criar reuniões/conferências acesse a aba **Conferências**, conforme mostra a Imagem 01.

<figure markdown>
<img src= "https://gitlab.com/lappis-unb/decidimbr/documentacao/-/raw/main/docs/assetsTutoriais/conferencias/AbaConferencia.JPG?ref_type=heads" alt=" Aba Conferência" style="float: none; margin: auto"> 
</figure> 
<p align="justify">
<h6 align = "center"> Imagem 01 - Aba Conferências (Fonte: DECIDIM 2024)</h6>
</p> 


Agora localize o botão "Nova Reunião" conforme imagem 02.
<figure markdown>
<img src= "https://gitlab.com/lappis-unb/decidimbr/documentacao/-/raw/main/docs/assetsTutoriais/conferencias/NovaConferencia.JPG?ref_type=heads" alt=" Criar Nova reunião" style="float: none; margin: auto"> 
</figure> 
<p align="justify">
<h6 align = "center"> Imagem 02 - Criar Nova reunião (Fonte: DECIDIM 2024)</h6>
</p> 


Nesta etapa (imagem 03) você deverá preencher todos os campos solicitados.

- <u>Título</u>

- <u>Descrição</u>

- <u>Tipo</u>:

    - Se for **presencial**:
        - Informe endereço;
        - Informe localização;
        - Informe dicas de localização;

    - Se for **virtual**:
        - Informe URL da reunião on-line; 
        - Informe tipo do iframe embutido;
            - Nenhum:
            - Embutir na página do evento:
            - Abrir URL em uma nova aba:
        - Nível de acesso do iframe:
            - Todos os visitantes;
            - Somente participantes inscritos;
            - Participantes registrados para este evento;

    - Se for **ambas**:
        - Informe endereço;
        - Informe localização;
        - Informe dicas de localização;
        - Informe URL da reunião on-line;
        - Informe tipo do iframe embutido;
            - Nenhum:
            - Embutir na página do evento:
            - Abrir URL em uma nova aba:
        - Nível de acesso do iframe:
            - Todos os visitantes;
            - Somente participantes inscritos;
            - Participantes registrados para este evento;

- <u>Hora de início</u>: informe a data e a hora que sua reunião terá início.

- <u>Hora de término</u>: informe a data e a hora que sua reunião será encerrada.

- <u>Categoria</u>;

- <u>Tipo de inscrição</u>;
    - inscrição desativada:
    - nesta plataforma:
    - em uma plataforma diferente:

Pronto, basta clicar no botão **Criar** e sua reunião foi gerada.

<figure markdown>
<img src= "https://gitlab.com/lappis-unb/decidimbr/documentacao/-/raw/main/docs/assetsTutoriais/conferencias/CriarReuniao.JPG?ref_type=heads" alt=" Criar Reunião" style="float: none; margin: auto"> 
</figure> 
<p align="justify">
<h6 align = "center"> Imagem 03 - Criar Reunião (Fonte: DECIDIM 2024)</h6>
</p> 


