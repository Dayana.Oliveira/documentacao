# Jornada do usuário cidadão 

O presente documento tem como objetivo explicar a jornada do usuário cidadão, exemplificando quais são as jornadas que o usuário deverá percorrer para poder participar de uma Conferência dentro da plataforma Brasil Participativo.

## Criando perfil do usuário

Para que você usuário cidadão possa participar de qualquer processo participativo na plataforma Brasil Participativo é necessário que crie o seu perfil. Para dar inicio ao seu cadastro, você deverá clicar na opção "**Criar perfil**" conforme Imagem 01.

<figure markdown>
<img src= "https://gitlab.com/lappis-unb/decidimbr/documentacao/-/raw/main/docs/assetsTutoriais/conferencias/CriarPerfil.JPG?ref_type=heads" alt=" Criar Perfil" style="float: none; margin: auto"> 
</figure> 
<p align="justify">
<h6 align = "center"> Imagem 01 - Criar Perfil (Fonte: DECIDIM 2024)</h6>
</p> 


Depois clique em conecte com o gov.br

<figure markdown>
<img src= "https://gitlab.com/lappis-unb/decidimbr/documentacao/-/raw/main/docs/assetsTutoriais/conferencias/ConecteSe.JPG?ref_type=heads" alt=" Conectar ao gov.Br" style="float: none; margin: auto"> 
</figure> 
<p align="justify">
<h6 align = "center"> Imagem 02 - Conectar ao gov.Br (Fonte: DECIDIM 2024)</h6>
</p> 

Após isto, preencha a tela (Imagem 03) informando seu CPF.

<figure markdown>
<img src= "https://gitlab.com/lappis-unb/decidimbr/documentacao/-/raw/main/docs/assetsTutoriais/conferencias/GovBr.JPG?ref_type=heads" alt="Informar CPF" style="float: none; margin: auto"> 
</figure> 
<p align="justify">
<h6 align = "center"> Imagem 03 - Informe seu CPF (Fonte: DECIDIM 2024)</h6>
</p> 

Após informar o seu CPF, agora vc deverá criar uma senha (Imagem 04)
<figure markdown>
<img src= "https://gitlab.com/lappis-unb/decidimbr/documentacao/-/raw/main/docs/assetsTutoriais/conferencias/DigitarSenhaGov.JPG?ref_type=heads" alt="Informar senha" style="float: none; margin: auto"> 
</figure> 
<p align="justify">
<h6 align = "center"> Imagem 04 - Informe senha (Fonte: DECIDIM 2024)</h6>
</p> 

Ficou com dúvida de como conectar-se com o gov.Br, acesse este link [Criar sua conta gov.br](https://www.gov.br/pt-br/servicos/criar-sua-conta-gov.br).

***********************************************************************************************************************************

## Criando EVENTOS (reuniões ou conferências)


O presente documento tem como objetivo orientar o usuário sobre como criar reuniões ou conferências, seja uma etapa municipal, estadual ou livre dentro da plataforma Brasil Participativo.

Para criar reuniões/conferências acesse a aba **Conferências**, conforme mostra a Imagem 01.

<figure markdown>
<img src= "https://gitlab.com/lappis-unb/decidimbr/documentacao/-/raw/main/docs/assetsTutoriais/conferencias/AbaConferencia.JPG?ref_type=heads" alt=" Aba Conferência" style="float: none; margin: auto"> 
</figure> 
<p align="justify">
<h6 align = "center"> Imagem 01 - Aba Conferências (Fonte: DECIDIM 2024)</h6>
</p> 


Agora localize o botão "Nova Reunião" conforme imagem 02.
<figure markdown>
<img src= "https://gitlab.com/lappis-unb/decidimbr/documentacao/-/raw/main/docs/assetsTutoriais/conferencias/NovaConferencia.JPG?ref_type=heads" alt=" Criar Nova reunião" style="float: none; margin: auto"> 
</figure> 
<p align="justify">
<h6 align = "center"> Imagem 02 - Criar Nova reunião (Fonte: DECIDIM 2024)</h6>
</p> 


Nesta etapa (imagem 03) você deverá preencher todos os campos solicitados.

- <u>Título</u>

- <u>Descrição</u>

- <u>Tipo</u>:

    - Se for **presencial**:
        - Informe endereço;
        - Informe localização;
        - Informe dicas de localização;

    - Se for **virtual**:
        - Informe URL da reunião on-line; 
        - Informe tipo do iframe embutido;
            - Nenhum:
            - Embutir na página do evento:
            - Abrir URL em uma nova aba:
        - Nível de acesso do iframe:
            - Todos os visitantes;
            - Somente participantes inscritos;
            - Participantes registrados para este evento;

    - Se for **ambas**:
        - Informe endereço;
        - Informe localização;
        - Informe dicas de localização;
        - Informe URL da reunião on-line;
        - Informe tipo do iframe embutido;
            - Nenhum:
            - Embutir na página do evento:
            - Abrir URL em uma nova aba:
        - Nível de acesso do iframe:
            - Todos os visitantes;
            - Somente participantes inscritos;
            - Participantes registrados para este evento;

- <u>Hora de início</u>: informe a data e a hora que sua reunião terá início.

- <u>Hora de término</u>: informe a data e a hora que sua reunião será encerrada.

- <u>Categoria</u>;

- <u>Tipo de inscrição</u>;
    - inscrição desativada:
    - nesta plataforma:
    - em uma plataforma diferente:

Pronto, basta clicar no botão **Criar** e sua reunião foi gerada.

<figure markdown>
<img src= "https://gitlab.com/lappis-unb/decidimbr/documentacao/-/raw/main/docs/assetsTutoriais/conferencias/CriarReuniao.JPG?ref_type=heads" alt=" Criar Reunião" style="float: none; margin: auto"> 
</figure> 
<p align="justify">
<h6 align = "center"> Imagem 03 - Criar Reunião (Fonte: DECIDIM 2024)</h6>
</p> 


## Participando de um evento Presencial



## Participando de um evento Online
- <u>Como participar de um evento online?</u>

- Selecione o botão conferência;
- Escolha qual conferência deseja participar; 
- Selecione o botão Inscreva-se; 
- Selecione o botão confirme; 
- Aguarde até sua participação ser validada por um moderador; 
- Pronto você está participando!;




## Participando de um evento Híbrido



## Criando Proposta


O presente documento tem como objetivo orientar o usuário sobre como se deve criar propostas em uma Conferência dentro da plataforma Brasil Participativo.

Para criar propostas acesse a aba Propostas/Etapa Digital e clique na opção conforme mostra a Imagem 01.

<figure markdown>
<img src= "https://gitlab.com/lappis-unb/decidimbr/documentacao/-/raw/main/docs/assetsTutoriais/conferencias/CriarProposta.png?ref_type=heads" alt="Criar Minha Proposta" style="float: none; margin: auto"> 
</figure> 
<p align="justify">
<h6 align = "center"> Imagem 01 - Criar Minha Proposta (Fonte: DECIDIM 2024)</h6>
</p> 


Após preencha o Título da Proposta e faça uma breve descrição do que vai se tratar a sua proposta. Após terminar o preenchimento você poderá visualizar a sua proposta clicando na opção Pré-visualizar conforme Imagem 02.

<figure markdown>
<img src= "https://gitlab.com/lappis-unb/decidimbr/documentacao/-/raw/main/docs/assetsTutoriais/conferencias/Previsualizar.png?ref_type=heads" alt=" Pré-visualizar Proposta" style="float: none; margin: auto"> 
</figure> 
<p align="justify">
<h6 align = "center"> Imagem 2 - Pré-visualizar Proposta (Fonte: DECIDIM 2024)</h6>
</p> 


Estando de acordo, agora você deverá publicar a sua proposta (Imagem 03).

<figure markdown>
<img src= "https://gitlab.com/lappis-unb/decidimbr/documentacao/-/raw/main/docs/assetsTutoriais/conferencias/Publicar_Proposta.JPG?ref_type=heads" alt=" Publicar Proposta" style="float: none; margin: auto"> 
</figure> 
<p align="justify">
<h6 align = "center"> Imagem 3 - Publicar Proposta (Fonte: DECIDIM 2024)</h6>
</p> 
